program -> declaration-list eof

declaration-list -> declaration-list_1

declaration-list_1 -> declaration declaration-list_1
declaration-list_1 -> ε

declaration -> int Fint
declaration -> void Fvoid

Fint -> ID FID_5

FID_5 -> FID_1
FID_5 -> ( params ) compound-stmt

Fvoid -> ID FID_6

FID_6 -> FID_1
FID_6 -> ( params ) compound-stmt

FID_1 -> ;
FID_1 -> [ NUM ] ;

type-specifier -> int
type-specifier -> void

params -> int Ftype-specifier_1 param-list_1
params -> void Fvoid_1

Fvoid_1 -> Ftype-specifier_1 param-list_1
Fvoid_1 -> ε

param-list_1 -> comma param param-list_1
param-list_1 -> ε

param -> type-specifier Ftype-specifier_1

Ftype-specifier_1 -> ID FID_2

FID_2 -> ε
FID_2 -> [ ]

compound-stmt -> { declaration-list statement-list }

statement-list -> statement-list_1

statement-list_1 -> statement statement-list_1
statement-list_1 -> ε

statement -> expression-stmt
statement -> compound-stmt
statement -> selection-stmt
statement -> iteration-stmt
statement -> return-stmt
statement -> switch-stmt

expression-stmt -> expression ;
expression-stmt -> continue ;
expression-stmt -> break ;
expression-stmt -> ;

selection-stmt -> if  ( expression ) statement else statement

iteration-stmt -> while  ( expression ) statement

return-stmt -> return Freturn

Freturn -> ;
Freturn -> expression ;

switch-stmt -> switch  ( expression ) { case-stmt default-stmt }

case-stmt -> case-stmt_1

case-stmt_1 -> case-stmt case-stmt_1
case-stmt_1 -> ε

case-stmt -> case NUM : statement-list

default-stmt -> default : statement-list
default-stmt -> ε

expression -> ID FID_3
expression -> ( expression ) term_1 additive-expression_1 Fadditive-expression
expression -> NUM term_1 additive-expression_1 Fadditive-expression
expression -> + factor term_1 additive-expression_1 Fadditive-expression
expression -> - factor term_1 additive-expression_1 Fadditive-expression

FID_3 -> FID FFID
FID_3 -> ( args ) term_1 additive-expression_1 Fadditive-expression

FFID -> = F=
FFID -> * single-factor term_1 additive-expression_1 Fadditive-expression
FFID -> addop term additive-expression_1 Fadditive-expression
FFID -> < additive-expression
FFID -> ε

F= -> expression
F= -> = additive-expression

FID -> ε
FID -> [ expression ]

Fadditive-expression -> RELOP additive-expression
Fadditive-expression -> ε

RELOP -> <
RELOP -> ==

additive-expression -> term additive-expression_1

additive-expression_1 -> addop term additive-expression_1
additive-expression_1 -> ε

addop -> +
addop -> -

term -> single-factor term_1

term_1 -> * single-factor term_1
term_1 -> ε

single-factor -> factor
single-factor -> + factor
single-factor -> - factor

factor ->  ( expression )
factor -> ID FID_4
factor -> NUM

FID_4 -> FID
FID_4 -> ( args )

args -> arg-list
args -> ε

arg-list -> expression arg-list_1

arg-list_1 -> comma expression arg-list_1
arg-list_1 -> ε



    int a = 0;
    // comment 2
    a = 2 + +2;
    a = a + -3;
    cde = a;
    if (b /* cmnt1 */ == 3) {
        a = 3;
        cd!e = -7;
    }
    else
    {
        b = a < cde;
        {cde = @2;
    }}